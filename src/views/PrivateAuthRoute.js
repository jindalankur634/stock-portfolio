import React from "react";
import { Navigate } from "react-router-dom";
import { toast } from "react-toastify";
// import {useSelector} from 'react-redux';

const PrivateAuthRoute = ({ children }) => {
  toast.configure()
  // const storedUseDetails = useSelector((state) => state.loginReducers.userDetails)

  function isLogin() {
    let token = localStorage.getItem("token");
    return token === "true" ? true : false;
  }
  if (isLogin() === false) {
    // toast.error("Please Login!!");
    return <Navigate to="/login" replace />;
  }

  return children;
};

export default PrivateAuthRoute;
