import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Card, CardBody, CardTitle, Col, Row } from "reactstrap";

const Login = () => {
  toast.configure();
  const navigate = useNavigate();
  const formData = {
    EMAIL: "",
    PASSWORD: "",
  };

  const keys = {
    EMAIL: "EMAIL",
    PASSWORD: "PASSWORD",
  };
  const [data, setData] = useState(formData);
  const [error, setError] = useState();
  const email = "admin@stock.com";
  const pass = "123456";

  useEffect(() => {
    localStorage.setItem("token", false);
  }, []);

  const handleChange = (event, text) => {
    const { target } = event;
    const value = target.value;
    const { name } = target;
    if (name == "EMAIL") {
      setData({ ...data, [name]: value });
    } else if (name == "PASSWORD") {
      setData({ ...data, [name]: value });
    }
  };

  const validate = () => {
    if (data.PASSWORD == "" && data.EMAIL == "") {
      toast.error("Please Enter credentials");
    } else if (data.EMAIL == "") {
      toast.error("Email is Required");
    } else if (data.PASSWORD == "") {
      toast.error("Password is Required");
    } else if ((data.EMAIL != email && data.PASSWORD != pass) || (data.EMAIL != email || data.PASSWORD != pass)) {
      toast.error("Invalid Credentials Entered");
    }
  };

  const handleLogin = (e) => {
    e.preventDefault();
    validate();
    if (data.EMAIL == email && data.PASSWORD == pass) {
      toast.success("Login Successfully");
      navigate("/");
      localStorage.setItem('token',true)
    } else {
      localStorage.setItem('token',false)
      navigate("/login");
    }
  };

  return (
    <div>
      <div>
        <Row className="display-4 col-12 d-flex justify-content-center mt-5">
          Login
        </Row>
        <Row className="col-12 mt-5 justify-content-center">
          <Col className="input-group-text col-1 d-flex justify-content-center">
            Email
          </Col>
          <Col className="col-3">
            <input
              type="text"
              class="form-control"
              placeholder="Username@stock.com"
              aria-label="Username"
              aria-describedby="addon-wrapping"
              name={keys.EMAIL}
              onChange={(event) => {
                handleChange(event);
              }}
            />
          </Col>
        </Row>
        <Row className="col-12 mt-2 justify-content-center">
          <Col className="input-group-text col-1 d-flex justify-content-center">
            Password
          </Col>
          <Col className="col-3">
            <input
              name={keys.PASSWORD}
              type="password"
              className="form-control"
              placeholder="Password"
              aria-label="password"
              aria-describedby="addon-wrapping"
              onChange={(event) => {
                handleChange(event);
              }}
            />
          </Col>
        </Row>
        <Row className="col-12 mt-2 justify-content-center">
          <button
            type="button"
            className="btn btn-success m-1 col-2 "
            onClick={(e) => {
              handleLogin(e);
            }}
          >
            Login
          </button>
        </Row>
      </div>
    </div>

    // <Card className="card w-100 card-body d-flex justify-content-center align-items-center border-0 mt-5 ">
    //   {console.log(data)}
    //   <h5 className="card-title display-6 ml-2">Login</h5>
    //   <div className="card border-0">
    //     <div class="input-group flex-nowrap  my-2 p-2">
    //       <span class="input-group-text" id="addon-wrapping">
    //         E-mail
    //       </span>
    //       <input
    //         type="text"
    //         class="form-control"
    //         placeholder="Username@stock.com"
    //         aria-label="Username"
    //         aria-describedby="addon-wrapping"
    //         name={keys.EMAIL}
    //         onChange={(event) => {
    //           handleChange(event);
    //         }}
    //       />
    //     </div>
    //     <div className="input-group flex-nowrap  my-2 p-2">
    //       <span className="input-group-text" id="addon-wrapping">
    //         Password
    //       </span>
    //       <input
    //         name={keys.PASSWORD}
    //         type="password"
    //         className="form-control"
    //         placeholder="Password"
    //         aria-label="password"
    //         aria-describedby="addon-wrapping"
    //         onChange={(event) => {
    //           handleChange(event);
    //         }}
    //       />
    //     </div>

    //     <div>
    //       <button
    //         type="button"
    //         className="btn btn-success m-1"
    //         onClick={(e) => {
    //           handleLogin(e);
    //         }}
    //       >
    //         Login
    //       </button>
    //       <button className="btn btn-danger m-1">Forget Password</button>
    //     </div>
    //   </div>
    // </Card>
  );
};

export default Login;
