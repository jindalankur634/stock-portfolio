import React, { useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  CardSubtitle,
  CardTitle,
  Button,
  Label,
  Input,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import Navbar from "./Navbar";
import Portfolio from "../views/Portfolio";
import stock from "../data/stock.json";
import { toast } from "react-toastify";
import GraphView from "./graphView";

const StockFullDetail = () => {
  const navigate = useNavigate();
  const [graphView, setGraphView] = useState(false);
  const [modal, setModal] = useState(false);
  const [compName, setCompName] = useState();
  const location = useLocation();
  const symbol = location.pathname.split("/")[2];
  const filterData = stock.filter((stock) => stock.symbol == symbol);

  console.log(filterData, "dataaa");
  const keys = {
    STOCK_NAME: "STOCK_NAME",
    QUANTITY: "QUANTITY",
    TODAY_PRICE: "TODAY_PRICE",
    YES_PRICE: "YES_PRICE",
  };

  const FormData = {
    QUANTITY: "",
    BUY_PRICE: "",
  };

  const [buttonToggle, setButtonToggle] = useState(false);
  const [data, setData] = useState(FormData);
  const token = localStorage.getItem("token");

  function showModal() {
    setModal(!modal);
  }

  const handleChange = (e) => {
    if (e.target.value < 0) {
      return;
    }
    let { target } = e;
    let value = target.value;
    let { name } = target;
    setData({ ...data, [name]: value });
  };

  function handlePayNow() {    
    if (data.QUANTITY > 0) {
      let prevStocks = localStorage.getItem("allstock");
      console.log(prevStocks, "PREV STOCKAS");
      let revData = [];
      if (prevStocks != null) {
        revData = JSON.parse(prevStocks);
      }

      let stockObj = {
        QUANTITY: data.QUANTITY,
        BUY_PRICE: filterData[0].today_price,
        COMPANY: filterData[0].company,
        SYMBOL: filterData[0].symbol,
      };
      revData.push(stockObj);
      localStorage.setItem("allstock", JSON.stringify(revData));
      toast.success("Stock Purchased Successfully");

      FormData.QUANTITY = "";
      navigate("/portfolio");
    } else {
      toast.error("Please Enter Quantity");
    }
  }

  function showGraph() {
    if (filterData != "") {
      setCompName(filterData[0].company);
    }
    console.log(compName);
    setGraphView(!graphView);
  }

  return (
    <div className="bg-dark fullviewOuterDiv overflow-auto">
      <Navbar />
      <Card className="m-5 p-2 bg-dark">
        <CardTitle className=" text-white d-flex">
          <Row className="col-12">
            <Col className="col-3 display-5">{filterData[0].company}</Col>
            <Col className="col-6">
              <Row className="">
                <Col className="h3 d-flex justify-content-end text-success">
                  Today's High :
                </Col>
                <Col className="h3 text-success">
                  {filterData[0].today_price}
                </Col>
              </Row>
            </Col>
          </Row>
        </CardTitle>
        <CardSubtitle className="h5 mt-2 text-white d-flex">
          <Row className="col-12">
            <Col className="col-3 ml-1">{filterData[0].symbol}</Col>
            <Col className="col-6">
              <Row className="">
                <Col className="h3 d-flex justify-content-end text-danger">
                  Yesterday's High :
                </Col>
                <Col className="h3 text-danger">
                  {" "}
                  {filterData[0].yesterday_price}
                </Col>
              </Row>
            </Col>
          </Row>
        </CardSubtitle>
        <CardBody className="text-white">
          <Row className="mt-3 h2 col-12 my-3">About</Row>
          <Row className="h5 font-weight-light col-7">
            {filterData[0].description}{" "}
          </Row>

          <Row className="mt-5">
            <button
              type="button"
              className="btn btn-success m-1 w-25"
              onClick={() => {
                showModal(!modal);
              }}
            >
              Buy
            </button>
            <button
              type="button"
              className="float-right m-1 btn btn-danger w-25"
              onClick={() => {
                showGraph();

                // navigate(`/graph/${filterData[0].company}`);
                // <GraphView graphView={graphView}  />
              }}
            >
              Chart View
            </button>
          </Row>
        </CardBody>
      </Card>
      {graphView == true ? (
        <GraphView
          setGraphView={setGraphView}
          graphView={graphView}
          compName={compName}
        />
      ) : (
        ""
      )}

      <Modal isOpen={modal} toggle={showModal}>
        <ModalHeader toggle={showModal}>Buy Stocks</ModalHeader>
        <ModalBody>
          <Row className="m-2">
            <Col className="col-6 d-flex justify-content-end">
              <h5>Price : </h5>
            </Col>
            <Col className="col-6">
              <h5>{filterData[0].today_price}</h5>
            </Col>
          </Row>
          <Row className="m-2">
            <Col className="col-6 d-flex justify-content-end">
              <h5> Quantity :</h5>
            </Col>
            <Col className="col-6">
              <Input
                className="w-100 d-flex justify-content-end"
                type="number"
                onChange={(e) => {
                  handleChange(e);
                }}
                name={keys.QUANTITY}
                min="0"
                // value = {Math.abs(data.QUANTITY)}
                step="any"
              />
            </Col>
          </Row>
          <Row className="m-2">
            <Col className="col-6 d-flex justify-content-end">
              <h5>Total Amt : </h5>
            </Col>
            <Col className="col-6">
              <h5>
                {/* {console.log(data.QUANTITY, "quannnnn")} */}
                {data.QUANTITY > 0
                  ? (data.QUANTITY * filterData[0].today_price).toFixed(2)
                  : "0"}
              </h5>
            </Col>
          </Row>
          <ModalFooter>
            <Button
              onClick={() => {
                handlePayNow();
              }}
            >
              Pay Now
            </Button>
          </ModalFooter>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default StockFullDetail;
