#What is this project trying to achieve?
This is a Stock-portfolio app. We can Buy Stocks of various Companies and can review thier daily statistics with thier Prices. There is a Graph view of every company where user can read the previous high's and low's of stock.


## Git URL for Clone 
open command-prompt in folder you want to clone this project and Enter below command\
Git Clone [https://gitlab.com/jindalankur634/stock-portfolio](https://gitlab.com/jindalankur634/stock-portfolio)


### `npm install`
Open the project in any IDE, open Terminal there and run the command `npm install`


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000/login](http://localhost:3000/login) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.


### `Credentials for Login`

E-Mail : admin@stock.com\
Password : 123456


#Now You are in the `stock-portfolio` app.
